#!/bin/sh
# A script to remove all binaries named main from all subdirectories.

find . -name main -type f -exec rm -f {} \;
find . -name **.exe -type f -exec rm -f {} \;
find . -name **.o -type f -exec rm -f {} \;

# Clear all folders named output from subdirectories.
find . -name output -type d -exec rm -rf {} \;

echo "Cleared all binaries from subdirectories."
