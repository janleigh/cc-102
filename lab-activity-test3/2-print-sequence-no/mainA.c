#include <stdio.h>

int main() {
    for (int i = 1; i <= 11; i++) {
		switch (i) {
			case 1:
				printf("1, ");
				break;
			case 2: case 10:
				printf("5, ");
				break;
			case 3: case 9:
				printf("2, ");
				break;
			case 4: case 8:
				printf("4, ");
				break;
			case 5: case 7:
				printf("3, ");
				break;
			case 11:
				printf("1\n");
				break;
		}
	}

	return 0;
}
