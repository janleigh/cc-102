#include <stdio.h>

int main() {
    int i = 1;

	do {
		switch (i) {
			case 1:
				printf("1, ");
				break;
			case 2: case 10:
				printf("5, ");
				break;
			case 3: case 9:
				printf("2, ");
				break;
			case 4: case 8:
				printf("4, ");
				break;
			case 5: case 7:
				printf("3, ");
				break;
			case 11:
				printf("1\n");
				break;
		}

		i++;
	} while (i <= 11);

	return 0;
}
