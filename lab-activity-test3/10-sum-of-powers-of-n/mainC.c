#include <stdio.h>
#include <math.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = 1, sum = 0;
	do {
		sum += pow(i, i);
		i++;
	} while (i <= n);

	printf("= %d\n", sum);
	
	return 0;
}
