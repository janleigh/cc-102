#include <stdio.h>
#include <math.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = 1, sum = 0;
	while (i <= n) {
		sum += pow(i, i);
		i++;
	}

	printf("= %d\n", sum);
	
	return 0;
}
