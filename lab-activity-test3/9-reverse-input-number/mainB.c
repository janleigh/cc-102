#include <stdio.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = n;
	long reverse = 0;
	// S: https://stackoverflow.com/questions/28572952/reverse-digits-of-an-integer
	while (i > 0) {
		reverse = reverse * 10 + i % 10;
		i /= 10;
	}

	printf("Reverse number: %ld\n", reverse);

	return 0;
}
