#include <stdio.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = 1, sum = 0;
	do {
		sum += i * i;
		i++;
	} while (i <= n);

	printf("= %d\n", sum);
	
	return 0;
}
