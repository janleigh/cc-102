#include <stdio.h>

int main() {
    int n = 1;
	
    do {
        int s;
        s = n * n;

        printf("%d %d\n", n, s);
        n++;
    } while (n <= 5);

	return 0;
}
