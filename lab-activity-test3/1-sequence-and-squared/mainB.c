#include <stdio.h>

int main() {
    int n = 1;
	
    while (n <= 5) {
        int s;
        s = n * n;

        printf("%d %d\n", n, s);
        n++;
    }

	return 0;
}
