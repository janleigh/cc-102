#include <stdio.h>

int main() {
	int n, factorial;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = 1;
	do {
		factorial = 1;
		int j = 1;
		while (j <= i) {
			factorial *= j;
			j++;
		}
		i++;
	} while (i <= n);

	printf("Factorial value: %d\n", factorial);

	return 0;
}
