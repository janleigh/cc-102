#include <stdio.h>

int main() {
	int n, factorial;
	printf("Enter a number: ");
	scanf("%d", &n);

	int i = 1;
	while (i <= n) {
		factorial = 1;
		int j = 1;
		while (j <= i) {
			factorial *= j;
			j++;
		}
		i++;
	}

	printf("Factorial value: %d\n", factorial);

	return 0;
}
