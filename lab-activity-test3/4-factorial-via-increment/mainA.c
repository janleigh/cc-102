#include <stdio.h>

int main() {
	int n, factorial;
	printf("Enter a number: ");
	scanf("%d", &n);

	for (int i = 1; i <= n; i++) {
		factorial = 1;
		for (int j = 1; j <= i; j++) {
			factorial *= j;
		}
	}

	printf("Factorial value: %d\n", factorial);

	return 0;
}
