#include <stdio.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	printf("Fibonacci sequence: ");

	int i = 1;
	while (i <= n) {
		int x = 0, y = 1, z;
		int j = 1;
		while (j <= i) {
			z = x + y;
			x = y;
			y = z;

			if (i == n) {
				if (j == i) {
					printf("%d\n", x);
				} else {
					printf("%d ", x);
				}
			}

			j++;
		}
		i++;
	}

	return 0;
}
