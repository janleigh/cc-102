#include <stdio.h>

int main() {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	printf("Fibonacci sequence: ");

	for (int i = 1; i <= n; i++) {
		int x = 0, y = 1, z;
		for (int j = 1; j <= i; j++) {
			z = x + y;
			x = y;
			y = z;

			if (i == n) {
				if (j == i) {
					printf("%d\n", x);
				} else {
					printf("%d ", x);
				}
			}
		}
	}

	return 0;
}
