#include <stdio.h>
#include <stdlib.h>

#define Pi 3.1416

// Module version
int main() {
	int r;
	float A;
	system("clear");
	printf("Enter the radius:");
	scanf("%d", &r);
	A = Pi * r * r;
	printf("\nThe area: %f", A);
	// getch();
}

int main(void) {
	float r, A;

	printf("Enter radius: ");
	scanf("%f", &r);

	A = Pi * r * r;
	printf("\nThe area: %f", A);

	return 0;
}
