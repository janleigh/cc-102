#include <stdio.h>
#include <stdlib.h>

// Module version
int main() {
	int sum, n1, n2;
	system("clear");
	printf("Enter two nos.\n");
	scanf("%d %d", &n1, &n2);
	sum = n1 + n2;
	printf("\nThe sum: %d", sum);
	// getch(); // THIS DOESN'T WORK IN LINUX!!! I use arch btw.
}

int main(void) {
	float sum, n1, n2;

	printf("Enter first number: ");
	scanf("%f", &n1);
	printf("Enter second number: ");
	scanf("%f", &n2);

	sum = n1 + n2;
	printf("\nThe sum: %.2f", sum);

	return 0;
}
