#include <stdio.h>

int is_even(int x) {
	return x % 2;
}

int main() {
	int n;
	printf("\nEnter a number: ");
	scanf("%d", &n);

	printf("\nThe number is %s\n", is_even(n) ? "odd" : "even");

	return 0;
}
