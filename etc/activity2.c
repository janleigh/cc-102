#include <stdio.h>
#define LIMIT 100

typedef struct student {
	char name[LIMIT];
	char course_and_year[LIMIT];
	char address[LIMIT];
	char birth_date[LIMIT];
} student;

void get_input(char* prompt, char* value) {
	printf("%s", prompt);
	scanf("%[^\n]%*c", value);
}

int main(void) {
	student s;

	get_input("\nInput name: ", s.name);
	get_input("Input course and year: ", s.course_and_year);
	get_input("Input address: ", s.address);
	get_input("Input birthdate: ", s.birth_date);

	printf("\nName: %s\nCourse & Year: %s\nAddress: %s\nBirthdate: %s\n", s.name, s.course_and_year, s.address, s.birth_date);

	return 0;
}
