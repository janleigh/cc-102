#include <stdio.h>

#define CASH_DISCOUNT 0.1
#define SECOND_INSTALLMENT_DISCOUNT 0.05
#define THIRD_INSTALLMENT_DISCOUNT CASH_DISCOUNT

int main() {
	int tuitionFee, paymentMethod;
	int discount;

	printf("Enter tuition fee: ");
	scanf("%d", &tuitionFee);

	printf("(Press 1 for Cash, 2 for Two Installment, 3 for Three Installment)\n");
	printf("Enter payment method: ");
	scanf("%d", &paymentMethod);

	if (paymentMethod == 1) {
		discount = tuitionFee * CASH_DISCOUNT;
	} else if (paymentMethod == 2) {
		discount = tuitionFee * SECOND_INSTALLMENT_DISCOUNT;
	} else if (paymentMethod == 3) {
		discount = tuitionFee * THIRD_INSTALLMENT_DISCOUNT;
	} else {
		printf("\nInvalid payment method\n");
		return 1;
	}

	printf("\nYour total tuition fee is: %d\n", tuitionFee - discount);
	return 0;
}
