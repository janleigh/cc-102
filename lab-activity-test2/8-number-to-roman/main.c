#include <stdio.h>
#include <string.h>

#define MAX_CHAR 10

int main() {
	int num, th, h, te, o;
	char roman[MAX_CHAR];

	printf("\nEnter number: ");
	scanf("%d", &num);
	
	// Tanginang module to. Max num 3000 tas maximum neto 3999.
	if (num > 1 && num < 3000) {
		if (num == 0) {
			printf("\nZero!\n");
			return 0;
		}

		char *thousands[] = {"", "M", "MM", "MMM"};
		char *hundreds[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
		char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
		char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};

		th = num / 1000;
		h = (num % 1000) / 100;
		te = (num % 100) / 10;
		o = num % 10;

		strcat(roman, thousands[th]);
		strcat(roman, hundreds[h]);
		strcat(roman, tens[te]);
		strcat(roman, ones[o]);

		// switch (th) {
		// 	case 1: strcat(roman, "M"); break;
		// 	case 2: strcat(roman, "MM"); break;
		// 	case 3: strcat(roman, "MMM"); break;
		// }

		// switch (h) {
		// 	case 1: strcat(roman, "C"); break;
		// 	case 2: strcat(roman, "CC"); break;
		// 	case 3: strcat(roman, "CCC"); break;
		// 	case 4: strcat(roman, "CD"); break;
		// 	case 5: strcat(roman, "D"); break;
		// 	case 6: strcat(roman, "DC"); break;
		// 	case 7: strcat(roman, "DCC"); break;
		// 	case 8: strcat(roman, "DCCC"); break;
		// 	case 9: strcat(roman, "CM"); break;
		// }

		// switch(te) {
		// 	case 1: strcat(roman, "X"); break;
		// 	case 2: strcat(roman, "XX"); break;
		// 	case 3: strcat(roman, "XXX"); break;
		// 	case 4: strcat(roman, "XL"); break;
		// 	case 5: strcat(roman, "L"); break;
		// 	case 6: strcat(roman, "LX"); break;
		// 	case 7: strcat(roman, "LXX"); break;
		// 	case 8: strcat(roman, "LXXX"); break;
		// 	case 9: strcat(roman, "XC"); break;
		// }

		// switch (o) {
		// 	case 1: strcat(roman, "I"); break;
		// 	case 2: strcat(roman, "II"); break;
		// 	case 3: strcat(roman, "III"); break;
		// 	case 4: strcat(roman, "IV"); break;
		// 	case 5: strcat(roman, "V"); break;
		// 	case 6: strcat(roman, "VI"); break;
		// 	case 7: strcat(roman, "VII"); break;
		// 	case 8: strcat(roman, "VIII"); break;
		// 	case 9: strcat(roman, "IX"); break;
		// }

		printf("\nRoman: %s\n", roman);
	} else {
		printf("\nOut of Range!\n");
		return 1;
	}

	return 0;
}
