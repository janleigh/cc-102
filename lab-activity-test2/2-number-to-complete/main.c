#include <stdio.h>

int main() {
	int month, day, year;
	char *monthName;

	printf("\nEnter a date in numerical form (ex. 2 26 1999): ");
	scanf("%d %d %d", &month, &day, &year);

	if (month < 1 || month > 12) {
		printf("\nInvalid month!\n");
		return 0;
	}

	// if (month == 1) {
	// 	monthName = "January";
	// } else if (month == 2) {
	// 	monthName = "February";
	// } else if (month == 3) {
	// 	monthName = "March";
	// } else if (month == 4) {
	// 	monthName = "April";
	// } else if (month == 5) {
	// 	monthName = "May";
	// } else if (month == 6) {
	// 	monthName = "June";
	// } else if (month == 7) {
	// 	monthName = "July";
	// } else if (month == 8) {
	// 	monthName = "August";
	// } else if (month == 9) {
	// 	monthName = "September";
	// } else if (month == 10) {
	// 	monthName = "October";
	// } else if (month == 11) {
	// 	monthName = "November";
	// } else if (month == 12) {
	// 	monthName = "December";
	// }

	switch (month) {
		case 1: monthName = "January"; break;
		case 2: monthName = "February"; break;
		case 3: monthName = "March"; break;
		case 4: monthName = "April"; break;
		case 5: monthName = "May"; break;
		case 6: monthName = "June"; break;
		case 7: monthName = "July"; break;
		case 8: monthName = "August"; break;
		case 9: monthName = "September"; break;
		case 10: monthName = "October"; break;
		case 11: monthName = "November"; break;
		case 12: monthName = "December"; break;
	}

	printf("\nComplete form: %s %d, %d\n", monthName, day, year);
}
