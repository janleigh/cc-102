#include <stdio.h>

int main() {
	int grade;
	float gradeEquivalent;

	printf("Enter grade: ");
	scanf("%d", &grade);

	if (grade >= 98 && grade <= 100) {
		gradeEquivalent = 1.0;
	} else if (grade >= 95 && grade <= 97) {
		gradeEquivalent = 1.25;
	} else if (grade >= 92 && grade <= 94) {
		gradeEquivalent = 1.5;
	} else if (grade >= 89 && grade <= 91) {
		gradeEquivalent = 1.75;
	} else if (grade >= 85 && grade <= 88) {
		gradeEquivalent = 2.0;
	} else if (grade >= 82 && grade <= 84) {
		gradeEquivalent = 2.25;
	} else if (grade >= 80 && grade <= 81) {
		gradeEquivalent = 2.5;
	} else if (grade >= 77 && grade <= 79) {
		gradeEquivalent = 2.75;
	} else if (grade >= 75 && grade <= 76) {
		gradeEquivalent = 3.0;
	} else {
		printf("\nInvalid grade\n");
		return 1;
	}

	printf("\nGrade equivalent: %.2f\n", gradeEquivalent);
	return 0;
}
