#include <stdio.h>
#include <string.h>

#define MAX_CHAR 100

int main() {
	int num, th, h, te, o;
	char words[MAX_CHAR];

	printf("\nEnter number: ");
	scanf("%d", &num);

	// Tanginang module to. Max num 3000 tas maximum neto 3999.
	if (num > 1 && num < 3000) {
		if (num == 0) {
			printf("\nZero!\n");
			return 0;
		}
        // Reference: https://www.w3schools.com/c/c_arrays.php
		char *thousands[] = {"", "one thousand ", "two thousand ", "three thousand "};
		char *hundreds[] = {"", "hundred ", "two hundred ", "three hundred ", "four hundred ", "five hundred ", "six hundred ", "seven hundred ", "eight hundred ", "nine hundred "};
		char *tens[] = {"", "ten ", "twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "};
		char *ones[] = {"", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine "};

		th = num / 1000;
		h = (num % 1000) / 100;
		te = (num % 100) / 10;
		o = num % 10;

		strcat(words, thousands[th]);
		strcat(words, hundreds[h]);
		strcat(words, tens[te]);
		strcat(words, ones[o]);

		// switch (th) {
		// 	case 1: strcat(words, "one thousand "); break;
		// 	case 2: strcat(words, "two thousand "); break;
		// 	case 3: strcat(words, "three thousand "); break;
		// }

		// switch (h) {
		// 	case 1: strcat(words, "one hundred "); break;
		// 	case 2: strcat(words, "two hundred "); break;
		// 	case 3: strcat(words, "three hundred "); break;
		// 	case 4: strcat(words, "four hundred "); break;
		// 	case 5: strcat(words, "five hundred "); break;
		// 	case 6: strcat(words, "six hundred "); break;
		// 	case 7: strcat(words, "seven hundred "); break;
		// 	case 8: strcat(words, "eight hundred "); break;
		// 	case 9: strcat(words, "nine hundred "); break;
		// }

		// switch (te) {
		// 	case 1: strcat(words, "ten "); break;
		// 	case 2: strcat(words, "twenty "); break;
		// 	case 3: strcat(words, "thirty "); break;
		// 	case 4: strcat(words, "forty "); break;
		// 	case 5: strcat(words, "fifty "); break;
		// 	case 6: strcat(words, "sixty "); break;
		// 	case 7: strcat(words, "seventy "); break;
		// 	case 8: strcat(words, "eighty "); break;
		// 	case 9: strcat(words, "ninety "); break;
		// }

		// switch (o) {
		// 	case 1: strcat(words, "one "); break;
		// 	case 2: strcat(words, "two "); break;
		// 	case 3: strcat(words, "three "); break;
		// 	case 4: strcat(words, "four "); break;
		// 	case 5: strcat(words, "five "); break;
		// 	case 6: strcat(words, "six "); break;
		// 	case 7: strcat(words, "seven "); break;
		// 	case 8: strcat(words, "eight "); break;
		// 	case 9: strcat(words, "nine "); break;
		// }

		printf("\nWord Equivalent: %s\n", words);
	} else {
		printf("\nOut of Range!\n");
		return 1;
	}

	return 0;
}
