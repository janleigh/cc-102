#include <stdio.h>

// NO. 7 SOLUTION
void exchange(int a, int b) {
	a = a + b;
	b = a - b;
	a = a - b;

	printf("a = %d, b = %d\n", a, b);
}

// NO. 5 SOLUTION
void exchange_with_temp(int a, int b) {
	int temp = a;
	a = b;
	b = temp;

	printf("a = %d, b = %d\n", a, b);
}

int main() {
	int a, b;

	printf("Enter two numbers: ");
	scanf("%d %d", &a, &b);

	printf("Before exchange: a = %d, b = %d\n", a, b);
	exchange(a, b);
	exchange_with_temp(a, b);

	return 0;
}
