#include <stdio.h>
#define Pi 3.1416

int main() {
	float circum, radius;

	printf("Enter circumference of circle: ");
	scanf("%f", &circum);

	radius = circum / (2 * Pi);

	printf("Radius of circle: %.4f\n", radius);
	return 0;
}
