#include <stdio.h>

int main() {
	float p, y, s, d;

	printf("Enter purchase price: ");
	scanf("%f", &p);

	printf("Enter expected years of service: ");
	scanf("%f", &y);

	printf("Enter salvage value: ");
	scanf("%f", &s);

	d = (p - s) / y;

	printf("Yearly Depreciation: %.2f\n", d);
	return 0;
}
