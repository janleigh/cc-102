#include <stdio.h>
// #include <math.h>

// kung wala kayong math.h, eto local implementation ng sqrt()
// Ref: https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method
// float sqrt(float x) {
// 	float y, z = x / 2;

// 	do {
// 		y = z;
// 		z = (y + (x / y)) / 2;
// 	}
	
// 	while (y != z); 
// 		return y;	
// }

void get_input(char* prompt, char* value) {
	printf("%s", prompt);
	scanf("%f", value);
}

int main() {
	float r, s, i, eoq;

	get_input("Enter total yearly production requirement: ", &r);
	get_input("Enter setup cost per order: ", &s);
	get_input("Enter inventory carrying cost: ", &i);

	eoq = 2 * r * s / i;
	eoq = sqrt(eoq);

	printf("Economic Order Quantity: %.3f\n", eoq);

	return 0;
}
