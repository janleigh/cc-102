#include <stdio.h>

int main() {
	float inch, cm;

	printf("Enter inch: ");
	scanf("%f", &inch);
	
	cm = inch * 2.54;
	printf("%.2f inch = %.2f cm\n", inch, cm);
	
	return 0;
}
