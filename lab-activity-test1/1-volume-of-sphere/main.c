#include <stdio.h>
#define Pi 3.1416

int main() {
	float r, volume;
	
	printf("Enter radius of sphere:\n");
	scanf("%f", &r);

	float exp = r * r * r;
	volume = (4.0 / 3.0) * Pi * exp;

	printf("Volume of sphere: %.4f\n", volume);
	return 0;
}
