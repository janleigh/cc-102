#include <stdio.h>

int main() {
	float fahrenheit, celsius;
	printf("Enter temperature in Celsius:\n");
	scanf("%f", &celsius);

	fahrenheit = (celsius * 9 / 5) + 32;

	printf("%.4f Celsius is %.4f Fahrenheit.\n", celsius, fahrenheit);
	return 0;
}
