#include <stdio.h>
#define Pi 3.1416

int main() {
	float radius, circum;

	printf("Enter radius of circle: ");
	scanf("%f", &radius);

	circum = 2 * Pi * radius;

	printf("Circumference of circle: %.4f\n", circum);
	return 0;
}
