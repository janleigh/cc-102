#include <stdio.h>
#define PER 51.50

int main() {
	float dollar, peso;
	printf("Enter amount in USD:\n");
	scanf("%f", &dollar);

	peso = dollar * PER;
	printf("%.2f USD is %.2f PHP.\n", dollar, peso);
	return 0;
}
